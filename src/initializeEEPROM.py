import os, sys, argparse, configparser

# Create argument parser
parser = argparse.ArgumentParser()

# Create config parser
config = configparser.ConfigParser()

# Setup arguments
parser.add_argument("-c", "--config", help="specifies path to config file")
parser.add_argument("-f", "--flash", action="store_true", help="upload flash")
parser.add_argument("-e", "--eeprom", action="store_true", help="upload eeprom")
parser.add_argument("-v", "--verbose", action="store_true", help="provides verbose output")
parser.add_argument("-s", "--save", action="store_true", help="saves provided configuration to defaults")
parser.add_argument("-aL", "--avrdude_location", help="specifies path to avrdude")
parser.add_argument("-aC", "--avrdude_configuration_location", help="specifies path to avrdude's configuration")
parser.add_argument("-aT", "--avrdude_programmer_type", help="specifies programmer type")
parser.add_argument("-aM", "--avrdude_part_type", help="specifies part type")
parser.add_argument("-aP", "--avrdude_port", help="specifies port")
parser.add_argument("-aB", "--avrdude_baud_rate", help="specifies baud rate")
parser.add_argument("-aF", "--avrdude_flash_location", help="specifies path to flash file to be programmed")
parser.add_argument("-aE", "--avrdude_eeprom_location", help="specifies path to eeprom file to be programmed")

# Store arguments
args = parser.parse_args()

# Check if initializeEEPROM configuration exists
if args.config is not None :
	# Store config location from argument
	config_location = args.config
else :
	# Make new folder if it doesn't exist
	if not os.path.isdir(os.path.expanduser("~/Documents/Arduino/build_custom")) :
		os.mkdir(os.path.expanduser("~/Documents/Arduino/build_custom"))
	
	# Copy default config to build_custom
	copy_command = 'cp '
	copy_command_src = os.getcwd() + "/src/default_config.ini"
	copy_command_dest = os.path.expanduser("~/Documents/Arduino/build_custom/config.ini")
	copy_command += "\"" + copy_command_src + "\" \"" + copy_command_dest + "\""
	print(copy_command)
	os.system(copy_command)

	# Store config location as default
	config_location = os.path.expanduser("~/Documents/Arduino/build_custom/config.ini")

# Check if configuration file exists
if not os.path.isfile(config_location) :
	print ("Requested configuration:", config_location)
	print("Configuration can't be found!")
	sys.exit(2)

# Read in configuration
config.read(config_location)

def parseArguments (argument, configurationSection, configurationVariable) :
	# Store temporarily from argument if provided
	if argument is not None :
		config[configurationSection][configurationVariable] = argument

parseArguments(args.avrdude_location, 'defaults', 'avrdude_location')
parseArguments(args.avrdude_configuration_location, 'defaults', 'avrdude_configuration_location')
parseArguments(args.avrdude_programmer_type, 'defaults', 'avrdude_programmer_type')
parseArguments(args.avrdude_part_type, 'defaults', 'avrdude_part_type')
parseArguments(args.avrdude_port, 'defaults', 'avrdude_port')
parseArguments(args.avrdude_baud_rate, 'defaults', 'avrdude_baud_rate')
parseArguments(args.avrdude_flash_location, 'defaults', 'avrdude_flash_location')
parseArguments(args.avrdude_eeprom_location, 'defaults', 'avrdude_eeprom_location')

def changeExtension(configurationSection, configurationVariable) :
	# Check current extension
	if os.path.isfile(os.path.splitext(config[configurationSection][configurationVariable])[1]) != '.hex' :

		# Get file base, directory and extension
		file_base = os.path.basename(config[configurationSection][configurationVariable])
		file_base = os.path.splitext(file_base)[0]
		file_dir = os.path.dirname(os.path.expanduser(config[configurationSection][configurationVariable]))

		# Concatenate parts of new file name
		file_name = file_dir + "/" + file_base + '.hex'

		print(config[configurationSection][configurationVariable], file_name)

		# Check if file exists
		if os.path.isfile(file_name) and os.path.expanduser(config[configurationSection][configurationVariable]) != file_name :
			print("Error while trying to rename file: File already exists.")
			sys.exit(2)

		# Change file type
		os.rename(config[configurationSection][configurationVariable], file_name)

	# Read in the file
	with open(file_name, 'r') as file :
		file_data = file.read()

	# Replace the target characters
	file_data = file_data.replace('\t', '') # Remove excess whitespace
	file_data = file_data.replace(',', '') # Remove commas
	file_data = file_data.replace('\n\n', '') # Remove duplicate empty lines

	# Write the file out again
	with open(file_name, 'w') as file :
		file.write(file_data)
	
# changeExtension('defaults', 'avrdude_flash_location')
changeExtension('defaults', 'avrdude_eeprom_location')

# Save new configuration to file if specified
if args.save :
	with open(config_location, 'w') as configfile:
		config.write(configfile)

# Concatenate final command to run
command = config['defaults']['avrdude_location']
command += " -v"
command += " -C " + config['defaults']['avrdude_configuration_location']
command += " -c " + config['defaults']['avrdude_programmer_type']
command += " -p " + config['defaults']['avrdude_part_type']
command += " -P " + config['defaults']['avrdude_port']
command += " -b " + config['defaults']['avrdude_baud_rate']

if args.flash : 
	print(" -Uflash:w:" + config['defaults']['avrdude_flash_location'] + ":i")
	command += " -Uflash:w:" + config['defaults']['avrdude_flash_location'] + ":i"

if args.eeprom : 
	print(" -Ueeprom:w:" + config['defaults']['avrdude_eeprom_location'] + ":i")
	command += " -Ueeprom:w:" + config['defaults']['avrdude_eeprom_location'] + ":i"

# Run final command
os.system(command)